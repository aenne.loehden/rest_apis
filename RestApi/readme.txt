'RestApi'

'harvestRestApi' harvests metadata from REST APIs. It harvests metadata either as XML or JSON, and stores the data as XML (transforming JSON to XML with org.json).

Because REST APIs are to be queried differently and expose different features, concrete conditions have to be investigated for the individual APIs and declared to the code.

REST APIS share some basic query parameters, like the quantity of records per result page, the sequential number of the start record, the format to display the records in.
The APIS differ in the names of those basic parameters, as well as default values and optional values for them. Also they differ in where to find query relevant data (like resumption token or total number of hits) within the result page. 
The basic parameters are required to loop through the result pages and decide whether to transform the format. They are to be declared in set functions.

Other parameters are quite specific to the single REST APIS (like e.g. in SHARE the desired source and publication type). Those are to be declared in a specific set function in the form of a query (e.g. for Glasstree via CrossRef 'filter=prefix:10.20850'). 

Some exemplary query URLs for different REST APIs:
# SocArXiv via SHARE
  https://share.osf.io/api/v2/search/creativeworks/_search?q=%28sources%3ASocArXiv+AND+type%3Apreprint%29&size=100&from=0&format=json
# EngrXiv via SHARE
  https://share.osf.io/api/v2/search/creativeworks/_search?q=q=%28sources%3AengrXiv+AND+type%3Apreprint%29&size=100&from=0&format=json
# PsyArXiv via SHARE
  https://share.osf.io/api/v2/search/creativeworks/_search?q=%28sources%3APsyArXiv+AND+type%3Apreprint%29&size=100&from=0&format=json
# Europe PubMed Central
  http://www.ebi.ac.uk/europepmc/webservices/rest/search?query=GRANT_AGENCY:%22European%20Research%20Council%22&resulttype=core&pageSize=100&cursorMark=0&format=xml
# Glasstree via Crossref
  http://api.crossref.org/works?filter=prefix:10.20850&rows=100&offset=0

In 'test' specifics are set for some datasources and REST APIs.

The errors within org.json (XMLTokener.java) can be ignored.