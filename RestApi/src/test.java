
public class test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/**
		harvestRestApi harvestShareSocArXiv = new harvestRestApi();
		harvestShareSocArXiv.setSourceName("SocArXiv via SHARE");
		harvestShareSocArXiv.setBaseUrl("https://share.osf.io/api/v2/search/creativeworks/_search");
		harvestShareSocArXiv.setResumption("count","from","");
		harvestShareSocArXiv.setResultTotal("//hits/total");
		harvestShareSocArXiv.setResultSize("size",100);
		harvestShareSocArXiv.setResultFormat("format","json");
		harvestShareSocArXiv.setQueryCont("q=%28sources%3ASocArXiv+AND+type%3Apreprint%29");
		harvestShareSocArXiv.queryLoop();
		*/
		
		/**
		harvestRestApi harvestShareEngrXiv = new harvestRestApi();
		harvestShareEngrXiv.setSourceName("engrXiv via SHARE");
		harvestShareEngrXiv.setBaseUrl("https://share.osf.io/api/v2/search/creativeworks/_search");
		harvestShareEngrXiv.setResumption("count","from","");
		harvestShareEngrXiv.setResultTotal("//hits/total");
		harvestShareEngrXiv.setResultSize("size",100);
		harvestShareEngrXiv.setResultFormat("format","json");
		harvestShareEngrXiv.setQueryCont("q=%28sources%3AengrXiv+AND+type%3Apreprint%29");
		harvestShareEngrXiv.queryLoop();
		*/
		
		/**
		harvestRestApi harvestSharePsyArXiv = new harvestRestApi();
		harvestSharePsyArXiv.setSourceName("PsyArXiv via SHARE");
		harvestSharePsyArXiv.setBaseUrl("https://share.osf.io/api/v2/search/creativeworks/_search");
		harvestSharePsyArXiv.setResumption("count","from","");
		harvestSharePsyArXiv.setResultTotal("//hits/total");
		harvestSharePsyArXiv.setResultSize("size",100);
		harvestSharePsyArXiv.setResultFormat("format","json");
		harvestSharePsyArXiv.setQueryCont("q=%28sources%3APsyArXiv+AND+type%3Apreprint%29");
		harvestSharePsyArXiv.queryLoop();
		*/
		
		/**
		harvestRestApi harvestEpmc = new harvestRestApi();
		harvestEpmc.setSourceName("Europe PubMed Central");
		harvestEpmc.setBaseUrl("http://www.ebi.ac.uk/europepmc/webservices/rest/search");
		harvestEpmc.setResumption("scan","cursorMark","//nextCursorMark");
		harvestEpmc.setResultTotal("//hitCount");
		harvestEpmc.setResultSize("pageSize",100);
		harvestEpmc.setResultFormat("format","xml");
		harvestEpmc.setQueryCont("query=GRANT_AGENCY:%22European%20Research%20Council%22&resulttype=core");
		harvestEpmc.queryLoop();
		*/
		
		///**
		harvestRestApi harvestCrossrefGlasstree = new harvestRestApi();
		harvestCrossrefGlasstree.setSourceName("Glasstree via Crossref");
		harvestCrossrefGlasstree.setBaseUrl("http://api.crossref.org/works");
		harvestCrossrefGlasstree.setResumption("count","offset","");
		harvestCrossrefGlasstree.setResultTotal("//total-results");
		harvestCrossrefGlasstree.setResultSize("rows",100);
		harvestCrossrefGlasstree.setResultFormat("","json");
		harvestCrossrefGlasstree.setQueryCont("filter=prefix:10.20850");
		harvestCrossrefGlasstree.queryLoop();
		//*/

	}

}
