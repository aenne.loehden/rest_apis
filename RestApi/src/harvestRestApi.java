import java.io.File;
import java.io.InputStream;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;


public class harvestRestApi {

	// parameters related to formal/technical search aspects
	String baseUrl = "";			// to be concatenated with query
	String format = "";				// result format (json, xml, dc, ...)
	String formatParam;				// query parameter name for format
	int resultSize = 100;			// number of records per result page
	String resultSizeParam = "";	// query parameter name for number of records per result page
	String resultTotalPath = "";	// result path to number of records deliverable for query
	String resumptionType;			// way of looping through result pages - scan (resumption token scanned from result), or count (start with resultFrom=0, add resultSize, loop until resultTotal)
	String resumptionParam = "";	// query parameter name for resumption token
	String resumptionPath = "";		// result path to resumption token for resumptionType=scan
	int resumptionInt = 0;			// integer resumption token (first record to harvest)
	String resumptionStr = Integer.toString(resumptionInt);  // string resumption token (first record to harvest or token scanned from results)
	
	// parameters related to content ... search aspects
	String queryCont = "";			// the actual query (including the query parameter)
	
	// parameters for storing results
	String sourceName = "";			// naming result storage directory, files

	// internal variables
	String query = "";				// the overall query, i.e. baseUrl + formal/technical search parameters + actual query
	String resultJson = "";			// result as json
	String resultXml = "";			// result as xml
	int resultTotal = 0;			// total number of hits for query
	String dumpDir = "RestApisDump/";		// directory for storing metadata of all datasources
	String dumpXml;					// directory for storing metadata of specific datasource
	
	/**
		
		parameters might be mandatory, optional, illicit for querying the APIs (parameters pertaining to this code being illicit for all APIs)
	 
		formal/technical parameters - examples for SHARE, EPMC:
			resumptionType: (no API parameter)
					* SHARE:	ill. | count (until: from + size >= resultTotal)
					* EPMC:		ill. | scan (until: nextCursorMark=cursorMark)
					* Crossref:	ill. | count
					> as a uniform loop end condition, from + size >= resultTotal is used, requiring the exposure of the total number of hits (resultTotalPath)
			resumptionParam:
					* SHARE:	opt. | from
					* EPMC:		opt. | cursorMark (initially void or * (0 works also))
					* Crossref:	opt. | offset
			resumptionPath:
					* SHARE:	ill. |
					* EPMC:		opt. | //nextCursorMark
					* Crossref:	ill. |
			resultSizeParam:
					* SHARE:	opt. | size
					* EPMC:		opt. | pageSize (default 25, max 1.000)
					* Crossref:	opt. | rows
			resultTotalPath:
					* SHARE:	ill. | //hits/total
					* EPMC:		ill. | //hitCount
					* Crossref:	ill. | //total-results
			format:
					* SHARE:	opt. | json
					* EPMC:		opt. | xml (XML, JSON, DC (default XML, funding info for XML, JSON, but not for DC)
					* Crossref:	ill. | json
			baseApi:
					* SHARE:	https://share.osf.io/api/v2/search/creativeworks/_search
					* EPMC: 
					* Crossref:	http://api.crossref.org/works
		
		content parameters - details for SHARE, EPMC
					* SHARE:	resulttype=core (full metadata for publication ID), lite (key metadata for search terms), idlist (IDs and sources for search terms) (default lite)
								query parameter name has to be 'q', 'query' is ignored
					* EPMC:		
					* Crossref:	filter=prefix:10.20850
					
	*/
	
	/**
		problems encountered:
			compilation problem:
				problem: Exception in thread "main" java.lang.Error: Unresolved compilation problem: The method iterator() of type JSONArray must override a superclass method (org.json.XML.toString(objJ))
				measure: project / properties / Java compiler / compiler compliance level -> 1.6 (from 1.5)
			json elements with spaces:
				problem: [Fatal Error] :1:141931: Mit Elementtyp "agent" verknüpfter Attributname "work" muss vom Zeichen " = " gefolgt werden.
						 after json->xml transformation, json elements with spaces get misinterpreted as xml elements with attributes
				measure: rid json elements of spaces
				
		potential problems which might be encountered:
			no total number of hits exposed:
				problem: the loop currently always relies on the total number of hits, which is exposed for all APIs so far
				measure: if an API exposes no number of total hits, the loop has to be made flexible
			result page too large:
				problem: {"status":500,"error":{"reason":"all shards failed","phase":"query","root_cause":[{"reason":"Result window is too large, from + size must be less than or equal to: [10000] but was [10100]. See the scroll api for a more efficient way to request large data sets. This limit can be set by changing the [index.max_result_window] index level parameter."
						 due to Elasticsearch
				measure: limit results per query or use scroll API
	 */

	public void setBaseUrl(String baseUrl){
		this.baseUrl = baseUrl;
	}
	
	public void setResumption(String resumptionType, String resumptionParam, String resumptionPath){
		this.resumptionType = resumptionType;
		this.resumptionParam = resumptionParam;
		this.resumptionPath = (resumptionPath=="") ? "/" : resumptionPath;
	}
	
	public void setResultTotal(String resultTotalPath){
		this.resultTotalPath = resultTotalPath;
	}
	
	public void setResultSize(String resultSizeParam, int resultSize){
		this.resultSizeParam = resultSizeParam;
		this.resultSize = resultSize;
	}
	
	public void setResultFormat(String formatParam, String format){
		this.formatParam = formatParam;
		this.format = format;
		
	}
	
	public void setQueryCont(String queryCont){
		this.queryCont = queryCont;
	}
	
	public void setSourceName(String sourceName){
		this.sourceName = sourceName;
	}

	public void queryLoop(){
		
		try {

			// prepare stuff for reading & storing files
			sourceName = sourceName.trim().replace(' ','_');
			dumpXml = dumpDir + sourceName + "/" + "dumpXml";
			// make directories for dumping files of a data source
			File dumpDirRoot = new File(dumpDir);						dumpDirRoot.mkdir();
			File dumpDirDatasource = new File(dumpDir + sourceName);	dumpDirDatasource.mkdir();
			File dumpDirDatasourceXml = new File(dumpXml);				dumpDirDatasourceXml.mkdir();
			InputStream resultStream;
					
			// prepare stuff for handling xml
			// prepare xml documents & xpaths to extract data from result
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document resultDoc = docBuilder.newDocument();
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression xprResultTotalPath = xpath.compile(resultTotalPath);
			XPathExpression xprResumptionPath = xpath.compile(resumptionPath);
			// prepare transformer to store data indented
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,"yes"); 
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount","3");
            
			// prepare query parts which might be illicit, optional, required
            String queryFormat = (formatParam!="")? "&" + formatParam + "=" + format : "";
            String querySize = (resultSizeParam!="")? "&" + resultSizeParam + "=" + resultSize : "";
            
            // query api initially to detect total number of records
            query = baseUrl + "?" + queryCont + querySize + "&" + resumptionParam + "=" + resumptionStr + queryFormat;
            resultStream = new URL(query).openStream();
			
			// if format=json, transform json 1:1/vanilla to xml, so that then it's only xml to handle
			if(format=="json"){				
				resultJson = IOUtils.toString(resultStream,"UTF-8");
				// pre-clean json - rid spaces of element names (misinterpreted as elements with attributes in xml)
				while(resultJson.matches(".*\"([^\"]*)\\s+([^\"]*)\":.*")){
					resultJson = resultJson.replaceAll("\"([^\"]*)\\s+([^\"]*)\":", "\"$1_$2\":");
				}
				org.json.JSONObject jsonObject = new org.json.JSONObject(resultJson);
				resultXml = org.json.XML.toString(jsonObject,"resultWrapOpenAIRE"); // wrap xml in single root element
				resultStream = IOUtils.toInputStream(resultXml,"UTF-8");
			}
			
			resultDoc = docBuilder.parse(resultStream);
			resultTotal = Integer.parseInt(xprResultTotalPath.evaluate(resultDoc));
			transformer.transform(new DOMSource(resultDoc), new StreamResult(new File(dumpDirDatasourceXml + "/" + sourceName +  "_" + resumptionInt + ".xml")));
			
			// loop through results, end condition number of harvested results vs. number of total results
			resumptionInt += resultSize;
			// prepare resumption token for query
			if(resumptionType=="scan"){ resumptionStr = xprResumptionPath.evaluate(resultDoc); }
			if(resumptionType=="count"){ resumptionStr = Integer.toString(resumptionInt); }
			while(resumptionInt < resultTotal){
				query = baseUrl + "?" + queryCont + querySize + "&" + resumptionParam + "=" + resumptionStr + queryFormat;
				resultStream = new URL(query).openStream();
				// if format=json, transform json 1:1/vanilla to xml, so that then it's only xml to handle
				if(format=="json"){
					resultJson = IOUtils.toString(resultStream,"UTF-8");
					while(resultJson.matches(".*\"([^\"]*)\\s+([^\"]*)\":.*")){
						resultJson = resultJson.replaceAll("\"([^\"]*)\\s+([^\"]*)\":", "\"$1_$2\":");
					}
					org.json.JSONObject jsonObject = new org.json.JSONObject(resultJson);
					resultXml = org.json.XML.toString(jsonObject,"resultWrapOpenAIRE");
					resultStream = IOUtils.toInputStream(resultXml,"UTF-8");
				}
				
				resultDoc = docBuilder.parse(resultStream);
				// store indented
				transformer.transform(new DOMSource(resultDoc), new StreamResult(new File(dumpDirDatasourceXml + "/" + sourceName +  "_" + resumptionInt + ".xml")));
				// prepare resumption token for query
				resumptionInt += resultSize;
				if(resumptionType=="scan"){ resumptionStr = xprResumptionPath.evaluate(resultDoc); }
				if(resumptionType=="count"){ resumptionStr = Integer.toString(resumptionInt); }
			}
			
		} catch(Exception ex) {ex.printStackTrace();}
		
	}
}
